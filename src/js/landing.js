$(function() {
  App.landing = {
    debounced(delay, fn) {
      let timerId;
      return function (...args) {
        if (timerId) {
          clearTimeout(timerId);
        }
        timerId = setTimeout(() => {
          fn(...args);
          timerId = null;
        }, delay);
      }
    },

    scroll_callback () {
      // get elements once and remember through clojure
      let start_scroll_el = $('#start-scroll-trigger');
      let stop_scroll_el = $('#stop-scroll-trigger');
      let sticky_element = $('#sticky-header');
      let sticky_class = 'get-started--active';

      // only that part will be called on scroll
      return function () {
        let start_scroll = start_scroll_el.offset().top;
        let stop_scroll = stop_scroll_el.offset().top;
        let current_position = $(this).scrollTop();

        if ((current_position > start_scroll) && (current_position < (stop_scroll - 70))){
          sticky_element.addClass(sticky_class)
        } else {
          sticky_element.removeClass(sticky_class)
        }
      };
    },

    sticky_header_bind() {
      if ($('#start-scroll-trigger').length === 0) { return false }

      // use debounce to handle only last event in events chain with the gap that less than delay
      $(window).scroll(this.debounced(25, this.scroll_callback()));
    },

    nav_bind() {
      let nav = $('.nav__list');

      if (nav.length === 0) { return false }

      let fast_animation_class = 'nav__list-dropdown--fade_fast';
      let slow_animation_class = 'nav__list-dropdown--fade_slow';

      nav.each(function () {
        let curr_nav = $(this);
        let popups = curr_nav.find('.popup');

        curr_nav.find('li').each(function() {
          $(this).on({
            mouseenter: function () {
              popups.addClass(fast_animation_class);
              popups.removeClass(slow_animation_class);
            },
            mouseleave: function () {
              popups.addClass(slow_animation_class);
              popups.removeClass(fast_animation_class);
            }
          });
        });
      });
    },

    init() {
      this.nav_bind();
      this.sticky_header_bind();
    }
  };

  App.landing.init();
});
