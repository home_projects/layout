# README #

## This is the Layout test

Technologies used:

* html5
* sass
* slim
* webpack

### How to set up ###

Install nmv

```curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.35.2/install.sh | bash```


Install node

```nvm install 13```
```nvm use 13```


Install dependencies

```npm install```


Run server

```npm start```

Build

```npm run build```
