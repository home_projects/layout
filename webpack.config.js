const path = require('path');

// SASS global import
const globImporter = require('node-sass-glob-importer');

// Extracted CSS
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

// To clean dist before every build
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

module.exports = {
  entry: [
    './src/js/main.js',
    './src/scss/main.scss',
    './src/index.slim',
  ],
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[name].js',
  },
  devServer: {
    contentBase: './dist',
  },
  module: {
    rules: [
      {
        test: /\.slim$/,
        use: [
          {
            loader: "file-loader",
            options: {
              name: "[name].html",
            }
          },
          "extract-loader",
          {
            loader: 'html-loader',
            options: {
              minimize: false,
            }
          },
          {
            loader: 'slim-lang-loader',
            options: {
              slimOptions: {
                'pretty': true
              }
            }
          }
        ]
      },
      {
        test: /\.scss$/,
          use: [
            {
              loader: MiniCssExtractPlugin.loader,
              options: {
                sourceMap: true
              },
            },
            {
              loader: 'css-loader',
              options: { sourceMap: true }
            },
            {
              loader: 'sass-loader',
              options: {
                sassOptions: {
                  importer: globImporter(),
                  sourceMap: true
                }
              },
            },
          ],
      },
      {
        test: /\.(png|svg|jpe?g|gif)$/,
        use: [
          'url-loader',
        ],
      },
    ],
  },
  plugins: [
    new MiniCssExtractPlugin(),
    new CleanWebpackPlugin(),
  ],
  devtool: "source-map"
};
